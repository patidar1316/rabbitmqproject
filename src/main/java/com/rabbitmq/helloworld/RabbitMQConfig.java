package com.rabbitmq.helloworld;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

	public static final String HELLOWORLD_QUEUE = "HELLOWORLD_QUEUE";

	@Bean
	public Queue queue() {
		return new Queue(HELLOWORLD_QUEUE);
	}

}
