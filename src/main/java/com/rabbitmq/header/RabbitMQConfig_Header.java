package com.rabbitmq.header;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.HeadersExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig_Header {

	public static final String H_MARKETING_QUEUE = "MARKETING_QUEUE";
	public static final String H_FINANCE_QUEUE = "FINANCE_QUEUE";
	public static final String H_ADMIN_QUEUE = "ADMIN_QUEUE";
	public static final String H_ALL_QUEUE = "ALL_QUEUE";


	public static final String HEADER_EXCHANGE = "HEADER_EXCHANGE";
	
	@Bean
	public Queue mQueue() {
		return new Queue(H_MARKETING_QUEUE);
	}
	
	@Bean
	public Queue fQueue() {
		return new Queue(H_FINANCE_QUEUE);
	}
	@Bean
	public Queue aQueue() {
		return new Queue(H_ADMIN_QUEUE);
	}


	@Bean
	public HeadersExchange headerExchange() {
		return new HeadersExchange(HEADER_EXCHANGE);
	}

	@Bean
	public Binding bindingExchangeWithQ1() {
		return BindingBuilder.bind(mQueue()).to(headerExchange()).where("department").matches("marketing");
	}
	@Bean
	public Binding bindingExchangeWithQ2() {
		return BindingBuilder.bind(aQueue()).to(headerExchange()).where("department").matches("admin");
	}
	@Bean
	public Binding bindingExchangeWithQ3() {
		return BindingBuilder.bind(fQueue()).to(headerExchange()).where("department").matches("finanace");
	}
}
