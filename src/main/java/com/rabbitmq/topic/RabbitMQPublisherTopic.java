package com.rabbitmq.topic;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rabbitmq.entity.Order;
import com.rabbitmq.entity.OrderDto;

@RestController
@RequestMapping("/topic")
public class RabbitMQPublisherTopic {

//	@Autowired
//	private AmqpTemplate amqpTemplate;
//
//	@GetMapping(value = "/producer")
//	public String producer(@RequestParam("exchangeName") String exchange, @RequestParam("routingKey") String routingKey,
//			@RequestParam("messageData") String messageData) {
//
//		amqpTemplate.convertAndSend(exchange, routingKey, messageData);
//
//		return "Message sent to the RabbitMQ Topic Exchange Successfully";
//	}

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@PostMapping("/order")
	public OrderDto placeOrder(@RequestBody Order order) {
		OrderDto orderDto = new OrderDto(order, "Using Topic type", "Hi Producer your oder is placed");
		rabbitTemplate.convertAndSend(RabbitMQConfig_Topic.TOPIC_EXCHANGE,"queue.marketing-r-key", orderDto);
		rabbitTemplate.convertAndSend(RabbitMQConfig_Topic.TOPIC_EXCHANGE,"queue.finanace-r-key", orderDto);
		rabbitTemplate.convertAndSend(RabbitMQConfig_Topic.TOPIC_EXCHANGE,"queue.admin-r-key", orderDto);
		rabbitTemplate.convertAndSend(RabbitMQConfig_Topic.TOPIC_EXCHANGE,"queue.*", orderDto);
		return orderDto;
	}
}
