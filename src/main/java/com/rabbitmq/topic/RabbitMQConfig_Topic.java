package com.rabbitmq.topic;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig_Topic {

	public static final String MARKETING_QUEUE = "MARKETING_QUEUE";
	public static final String FINANCE_QUEUE = "FINANCE_QUEUE";
	public static final String ADMIN_QUEUE = "ADMIN_QUEUE";
	public static final String ALL_QUEUE = "ALL_QUEUE";


	public static final String TOPIC_EXCHANGE = "TOPIC_EXCHANGE";
	
	@Bean
	public Queue mQueue() {
		return new Queue(MARKETING_QUEUE);
	}
	
	@Bean
	public Queue fQueue() {
		return new Queue(FINANCE_QUEUE);
	}
	@Bean
	public Queue aQueue() {
		return new Queue(ADMIN_QUEUE);
	}
	@Bean
	public Queue allQueue() {
		return new Queue(ALL_QUEUE);
	}

	@Bean
	public TopicExchange topicExchange() {
		return new TopicExchange(TOPIC_EXCHANGE);
	}

	@Bean
	public Binding bindingExchangeWithQ1() {
		return BindingBuilder.bind(mQueue()).to(topicExchange()).with("queue.marketing_r_key");
	}
	@Bean
	public Binding bindingExchangeWithQ2() {
		return BindingBuilder.bind(aQueue()).to(topicExchange()).with("queue.admin-r-key");
	}
	@Bean
	public Binding bindingExchangeWithQ3() {
		return BindingBuilder.bind(fQueue()).to(topicExchange()).with("queue.finanace-r-key");
	}
	@Bean
	public Binding bindingExchangeWithQ4() {
		return BindingBuilder.bind(allQueue()).to(topicExchange()).with("queue.*");
	}
}
