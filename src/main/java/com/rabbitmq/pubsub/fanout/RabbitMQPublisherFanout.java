package com.rabbitmq.pubsub.fanout;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rabbitmq.entity.Order;
import com.rabbitmq.entity.OrderDto;

@RestController
@RequestMapping("/fanOut")
public class RabbitMQPublisherFanout {

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@PostMapping("/order")
	public OrderDto placeOrder(@RequestBody Order order) {
		OrderDto orderDto = new OrderDto(order, "Using Fanout type", "Hi Producer your oder is placed");
		rabbitTemplate.convertAndSend(RabbitMQConfig_Fanout.FANOUT_EXCHANGE,"", orderDto);
		return orderDto;
	}
}
