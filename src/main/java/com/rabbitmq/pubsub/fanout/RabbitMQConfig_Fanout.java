package com.rabbitmq.pubsub.fanout;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig_Fanout {

	public static final String FANOUT_QUEUE1 = "FANOUT_QUEUE1";
	public static final String FANOUT_QUEUE2 = "FANOUT_QUEUE2";

	public static final String FANOUT_EXCHANGE = "FANOUT_EXCHANGE";

	@Bean
	public Queue queue1() {
		return new Queue(FANOUT_QUEUE1);
	}
	
	@Bean
	public Queue queue2() {
		return new Queue(FANOUT_QUEUE2);
	}

	@Bean
	public FanoutExchange fanoutEchange() {
		return new FanoutExchange(FANOUT_EXCHANGE);
	}

	@Bean
	public Binding bindingExchangeWithQ1() {
		return BindingBuilder.bind(queue1()).to(fanoutEchange());
	}
	@Bean
	public Binding bindingExchangeWithQ2() {
		return BindingBuilder.bind(queue2()).to(fanoutEchange());
	}

}
