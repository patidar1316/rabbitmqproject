package com.rabbitmq.pubsub.direct;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig_Direct {

	public static final String PUBLISHSUBSCRIBE_DIRECT_QUEUE1 = "PUBLISHSUBSCRIBE_DIRECT_QUEUE1";
	public static final String PUBLISHSUBSCRIBE_DIRECT_QUEUE2 = "PUBLISHSUBSCRIBE_DIRECT_QUEUE2";
	public static final String PUBLISHSUBSCRIBE_DIRECT_QUEUE3 = "PUBLISHSUBSCRIBE_DIRECT_QUEUE3";

	public static final String PUBLISHSUBSCRIBE_DIRECT_EXCHANGE = "PUBLISHSUBSCRIBE_DIRECT_EXCHANGE";

	public static final String PUBLISHSUBSCRIBE_DIRECT_ROUTING_KEY1 = "PUBLISHSUBSCRIBE_DIRECT_ROUTING_KEY1";
	public static final String PUBLISHSUBSCRIBE_DIRECT_ROUTING_KEY2 = "PUBLISHSUBSCRIBE_DIRECT_ROUTING_KEY2";

	@Bean
	public Queue queue1() {
		return new Queue(PUBLISHSUBSCRIBE_DIRECT_QUEUE1);
	}

	@Bean
	public Queue queue2() {
		return new Queue(PUBLISHSUBSCRIBE_DIRECT_QUEUE2);
	}

	@Bean
	public Queue queue3() {
		return new Queue(PUBLISHSUBSCRIBE_DIRECT_QUEUE3);
	}

	@Bean
	public DirectExchange directExchange() {
		return new DirectExchange(PUBLISHSUBSCRIBE_DIRECT_EXCHANGE);
	}

	@Bean
	public Binding bindingExchangeWithQ1() {
		return BindingBuilder.bind(queue1()).to(directExchange()).with(PUBLISHSUBSCRIBE_DIRECT_ROUTING_KEY1);
	}

	@Bean
	public Binding bindingExchangeWithQ2() {
		return BindingBuilder.bind(queue2()).to(directExchange()).with(PUBLISHSUBSCRIBE_DIRECT_ROUTING_KEY2);
	}

	@Bean
	public Binding bindingExchangeWithQ3() {
		return BindingBuilder.bind(queue3()).to(directExchange()).with(PUBLISHSUBSCRIBE_DIRECT_ROUTING_KEY1);
	}

}
