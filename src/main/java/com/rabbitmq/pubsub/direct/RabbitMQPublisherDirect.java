package com.rabbitmq.pubsub.direct;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rabbitmq.entity.Order;
import com.rabbitmq.entity.OrderDto;

@RestController
@RequestMapping("/pubSub")
public class RabbitMQPublisherDirect {

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@PostMapping("/order")
	public OrderDto placeOrder(@RequestBody Order order) {
		OrderDto orderDto = new OrderDto(order, "Using Direct type", "Hi Producer your oder is placed");
		rabbitTemplate.convertAndSend(RabbitMQConfig_Direct.PUBLISHSUBSCRIBE_DIRECT_EXCHANGE,RabbitMQConfig_Direct.PUBLISHSUBSCRIBE_DIRECT_ROUTING_KEY1, orderDto);
		rabbitTemplate.convertAndSend(RabbitMQConfig_Direct.PUBLISHSUBSCRIBE_DIRECT_EXCHANGE,RabbitMQConfig_Direct.PUBLISHSUBSCRIBE_DIRECT_ROUTING_KEY2, orderDto);

		return orderDto;
	}
}
